package ru.t1.nkiryukhin.tm.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;
import ru.t1.nkiryukhin.tm.enumerated.Status;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;

@Getter
@Setter
@Entity
@Table(name = "tm_project")
@NoArgsConstructor
@AllArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "project", propOrder = {
        "id",
        "name",
        "description",
        "created",
        "status"
})
public final class Project implements Serializable {

    @Id
    @NotNull
    @XmlElement(required = true)
    private String id = UUID.randomUUID().toString();

    @NotNull
    @XmlElement(required = true)
    private String name;

    @Nullable
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @XmlElement
    private Date created = new Date();

    @NotNull
    @XmlElement
    private String description = "";

    @NotNull
    @Enumerated(EnumType.STRING)
    @XmlElement
    private Status status = Status.NOT_STARTED;

    public Project(final @NotNull String name) {
        this.name = name;
    }

    public Project(final @NotNull String name, final @NotNull String description) {
        this.name = name;
        this.description = description;
    }

    public Project(
            @NotNull final String name,
            @NotNull final String description,
            @NotNull final Status status
    ) {
        this.name = name;
        this.description = description;
        this.status = status;
    }

    @Override
    public String toString() {
        return id + " : " + name + " : " + description + " : " + status.getDisplayName();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Project project = (Project) o;
        return Objects.equals(getId(), project.getId())
                && Objects.equals(created, project.created)
                && Objects.equals(description, project.description)
                && name.equals(project.name)
                && status == project.status;
    }

}