package ru.t1.nkiryukhin.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nkiryukhin.tm.model.Task;

import java.util.Collection;
import java.util.List;

public interface TaskRestEndpoint {

    void clear();

    long count();

    void delete(@NotNull Task task);

    void deleteById(@NotNull String id);

    void deleteList(List<Task> tasks);

    boolean existsById(@NotNull String id);

    @Nullable
    Collection<Task> findAll();

    @Nullable
    Task findById(@NotNull String id);

    void save(@NotNull Task task);

}