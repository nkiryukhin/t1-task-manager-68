package ru.t1.nkiryukhin.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.web.bind.annotation.RequestBody;
import ru.t1.nkiryukhin.tm.model.Project;

import java.util.Collection;
import java.util.List;

public interface ProjectRestEndpoint {

    void clear();

    long count() throws Exception;

    void delete(@NotNull Project project);

    void deleteById(@NotNull String id);

    void deleteList(List<Project> projects);

    boolean existsById(@NotNull String id);

    @Nullable
    Collection<Project> findAll();

    @Nullable
    Project findById(@NotNull String id);

    void save(@RequestBody @NotNull Project project);

}