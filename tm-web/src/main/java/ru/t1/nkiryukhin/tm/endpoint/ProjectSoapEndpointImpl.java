package ru.t1.nkiryukhin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.t1.nkiryukhin.tm.dto.soap.*;
import ru.t1.nkiryukhin.tm.repository.ProjectRepository;


@Endpoint
public class ProjectSoapEndpointImpl {

    @Autowired
    private ProjectRepository projectRepository;

    public final static String LOCATION_URI = "/ws";

    public final static String PORT_TYPE_NAME = "ProjectSoapEndpointPort";

    public final static String NAMESPACE = "http://nkiryukhin.t1.ru/tm/dto/soap";

    @ResponsePayload
    @PayloadRoot(localPart = "projectClearRequest", namespace = NAMESPACE)
    public ProjectClearResponse clear(@RequestPayload final ProjectClearRequest request) {
        projectRepository.deleteAll();
        return new ProjectClearResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectCountRequest", namespace = NAMESPACE)
    public ProjectCountResponse count(@RequestPayload final ProjectCountRequest request) {
        return new ProjectCountResponse(projectRepository.count());
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectDeleteByIdRequest", namespace = NAMESPACE)
    public ProjectDeleteByIdResponse deleteById(@RequestPayload final ProjectDeleteByIdRequest request) {
        projectRepository.deleteById(request.getId());
        return new ProjectDeleteByIdResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectDeleteListRequest", namespace = NAMESPACE)
    public ProjectDeleteListResponse deleteList(@RequestPayload final ProjectDeleteListRequest request) {
        projectRepository.deleteAll(request.getProjects());
        return new ProjectDeleteListResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectDeleteRequest", namespace = NAMESPACE)
    public ProjectDeleteResponse delete(@RequestPayload final ProjectDeleteRequest request) {
        projectRepository.deleteById(request.getProject().getId());
        return new ProjectDeleteResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectExistsByIdRequest", namespace = NAMESPACE)
    public ProjectExistsByIdResponse existsById(@RequestPayload final ProjectExistsByIdRequest request) {
        return new ProjectExistsByIdResponse(projectRepository.findById(request.getId()).isPresent());
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectFindAllRequest", namespace = NAMESPACE)
    public ProjectFindAllResponse findAll(@RequestPayload final ProjectFindAllRequest request) {
        return new ProjectFindAllResponse(projectRepository.findAll());
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectFindByIdRequest", namespace = NAMESPACE)
    public ProjectFindByIdResponse findById(@RequestPayload @NotNull final ProjectFindByIdRequest request) {
        return new ProjectFindByIdResponse(projectRepository.findById(request.getId()).orElse(null));
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectSaveRequest", namespace = NAMESPACE)
    public ProjectSaveResponse save(@RequestPayload @NotNull final ProjectSaveRequest request) {
        projectRepository.save(request.getProject());
        return new ProjectSaveResponse();
    }

}