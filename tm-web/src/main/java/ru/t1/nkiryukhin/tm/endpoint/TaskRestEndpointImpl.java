package ru.t1.nkiryukhin.tm.endpoint;


import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.nkiryukhin.tm.api.TaskRestEndpoint;
import ru.t1.nkiryukhin.tm.model.Task;
import ru.t1.nkiryukhin.tm.repository.TaskRepository;

import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("/api/tasks")
public class TaskRestEndpointImpl implements TaskRestEndpoint {

    @Autowired
    private TaskRepository taskRepository;

    @Override
    @PostMapping("/clear")
    public void clear() {
        taskRepository.deleteAll();
    }

    @Override
    @GetMapping("/count")
    public long count() {
        return taskRepository.count();
    }

    @Override
    @PostMapping("/deleteById/{id}")
    public void deleteById(@PathVariable("id") @NotNull final String id) {
        taskRepository.deleteById(id);
    }

    @Override
    @PostMapping("/delete")
    public void delete(@RequestBody @NotNull final Task task) {
        taskRepository.deleteById(task.getId());
    }

    @Override
    @PostMapping("/deleteList")
    public void deleteList(@RequestBody @NotNull List<Task> tasks) {
        for (Task task : tasks) {
            taskRepository.deleteById(task.getId());
        }
    }

    @Override
    @GetMapping("/existsById/{id}")
    public boolean existsById(@PathVariable("id") @NotNull final String id) {
        return taskRepository.findById(id).isPresent();
    }

    @Override
    @GetMapping("/findAll")
    public Collection<Task> findAll() {
        return taskRepository.findAll();
    }

    @Nullable
    @Override
    @GetMapping("/findById/{id}")
    public Task findById(@PathVariable("id") @NotNull final String id) {
        return taskRepository.findById(id).orElse(null);
    }

    @Override
    @PostMapping("/save")
    public void save(@RequestBody @NotNull final Task task) {
        taskRepository.save(task);
    }

}