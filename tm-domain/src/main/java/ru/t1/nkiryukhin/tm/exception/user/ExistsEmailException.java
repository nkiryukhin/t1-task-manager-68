package ru.t1.nkiryukhin.tm.exception.user;

public final class ExistsEmailException extends AbstractUserException {

    public ExistsEmailException() {
        super("Error! Email already exists...");
    }

}
