package ru.t1.nkiryukhin.tm.repository.dto;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.nkiryukhin.tm.dto.model.AbstractUserOwnedModelDTO;

@Repository
@Scope("prototype")
public interface AbstractUserOwnedDTORepository<M extends AbstractUserOwnedModelDTO> extends AbstractDTORepository<M> {
}