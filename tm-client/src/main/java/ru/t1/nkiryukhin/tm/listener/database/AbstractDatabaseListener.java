package ru.t1.nkiryukhin.tm.listener.database;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.nkiryukhin.tm.api.endpoint.IAdminEndpoint;
import ru.t1.nkiryukhin.tm.api.service.ILoggerService;
import ru.t1.nkiryukhin.tm.enumerated.Role;
import ru.t1.nkiryukhin.tm.listener.AbstractListener;

@Component
public abstract class AbstractDatabaseListener extends AbstractListener {

    @NotNull
    @Autowired
    protected IAdminEndpoint adminEndpoint;

    @NotNull
    @Autowired
    protected ILoggerService loggerService;

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}